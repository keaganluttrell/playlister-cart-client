FROM node:12-alpine

WORKDIR /

COPY . /

RUN npm install

RUN npm run build

# CMD ["npm", "start"]