import React from 'react';
import ReactDOM from 'react-dom';
import Cart from './components/cart.jsx';

ReactDOM.render(<Cart />, document.getElementById('cart-app'));