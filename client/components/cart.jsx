import '../styles/cart.css'
import React from 'react';
import { useEffect, useState } from 'react';
import axios from 'axios';

export default function Cart() {

    const [cart, setCart] = useState([]);

    function getCart() {
        axios.get("api/cart")
            .then(res => {
                const items = res.data.cart;
                console.log(res.data.cart)
                setCart(items);
            })
    }

    useEffect(() => getCart(), []);

    return (
        <div id="cart-list">
            <header>Cart</header>
            <div>{cart.map((item, index) => {
                const { title, artist, album } = item;
                return <div key={index + title}>{`${title}, ${artist}, ${album}`}</div>
            })}</div>
        </div>
    );
}